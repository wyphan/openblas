Document: openblas
Title: Debian openblas Manual
Author: <insert document author here>
Abstract: This manual describes what openblas is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/openblas/openblas.sgml.gz

Format: postscript
Files: /usr/share/doc/openblas/openblas.ps.gz

Format: text
Files: /usr/share/doc/openblas/openblas.text.gz

Format: HTML
Index: /usr/share/doc/openblas/html/index.html
Files: /usr/share/doc/openblas/html/*.html
